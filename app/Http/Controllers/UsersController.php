<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index(Request $request) {
        $users = User::find($request->input('id'));
        return $users;
    }

    public function show(User $user){
        return $user;
    }
}
