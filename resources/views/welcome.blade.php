<x-layout>
    <h1>Hi</h1>

    <x-slot name="slot1">
        Hello world
    </x-slot>

    <x-slot name="slot2">
        This is title
    </x-slot>

    <x-slot name="slot3">
        This is a description
    </x-slot>

    <div class="autoplay">
        <div><h3>1</h3></div>
        <div><h3>2</h3></div>
        <div><h3>3</h3></div>
        <div><h3>4</h3></div>
        <div><h3>5</h3></div>
        <div><h3>6</h3></div>
    </div>
</x-layout>
